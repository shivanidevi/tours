<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;
//  use JWTAuth;
use JWT;
use Auth;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Models\City;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function index(RegisterRequest $request)
    {
        $user = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password' => Hash::make($request->password),
                    ]);
        $user = User::first();
        $token = JWTAuth::login($user);
        return response()->json(['status' => 'success','message' => 'User created successfully','user'  => $user ,
            'authorisation' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ]);

    }
    public function login(LoginRequest $request)
    {

        $credentials = $request->only('email', 'password');

        $token = Auth::attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = Auth::user();
        return response()->json([
         'status' => 'success',
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ]);

    }

    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }
    
    public function city(Request $request)
    {
        $city = City::all();
        return response()->json(['status' => true, 'data' => $city ],201);

    }
    public function cities($id)
    {      
        $city = City::where('id',$id)->get();
        return response()->json(['status' => true, "data"=>$city, 'message' => 'single data']);
        }

}
 

